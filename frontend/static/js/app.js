// function init() {
// 	window.init();
// }

var isDebug = true;

//var isDebug = false;
var host = 'https://nixgonspace.appspot.com';
if (isDebug) {
  host = 'http://localhost:8080';
}

var nixgonspace = angular.module('nixgonspace.app', ['ngCart', 'ngCookies', 'ngMaterial', 'ngRoute', 'mdDateTime', 'pascalprecht.translate','materialDatePicker']);

// nixgonspace.config(function($mdThemingProvider) {
// 	$mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
// });

nixgonspace.config(['$translateProvider', function ($translateProvider) {
  // Simply register translation table as object hash
  $translateProvider.useStaticFilesLoader({
    prefix: 'json/local-',
    suffix: '.json'
  });

  $translateProvider.use('KR');
}]);

nixgonspace.config(function($routeProvider) {
  $routeProvider.when('/store', {
    controller: 'storeCtrl',
    templateUrl: 'partials/store.html'
  })
      .when('/store/:rocketId', {
        controller: 'rocketCtrl',
        templateUrl: 'partials/space-select.html'
      })
      .when('/store/:rocketId/:spaceId', {
        controller: 'durationCtrl',
        templateUrl: 'partials/start-select.html'
      })
      .when('/store/:rocketId/:spaceId/:startHour', {
        controller: 'durationCtrl',
        templateUrl: 'partials/duration-select.html'
      })
      .when('/signin', {
        controller: 'AppCtrl',
        templateUrl: 'partials/sign_in.html'
      })
      .when('/checkout', {
        controller: 'checkOutCtrl',
        templateUrl: 'partials/checkout.html'
      })
      .when('/cart', {
        controller: 'AppCtrl',
        templateUrl: 'partials/cart.html'
      })
      .when('/orders', {
        controller: 'AppCtrl',
        templateUrl: 'partials/orders.html'
      })
      .when('/periods', {
        controller: 'AppCtrl',
        templateUrl: 'partials/periods.html'
      })
      .when('/user', {
        controller: 'AppCtrl',
        templateUrl: 'partials/user.html'
      })
      .when('/signup', {
        controller: 'AppCtrl',
        templateUrl: 'partials/signup.html'
      })
      .when('/date', {
        controller: 'AppCtrl',
        templateUrl: 'partials/date.html'
      })
      .when('/payrequest', {
        controller: 'checkOutCtrl',
        templateUrl: 'partials/payrequest.html'
      })
      .otherwise({
        redirectTo: '/store'
      });
  moment.locale('ko-KR');
});

nixgonspace.factory('users', function() {
  var service = {};
  service.astronaut = {};
  service.setUser = function(user) {
    this.user = user;
  };
  return service;
});

nixgonspace.factory('booking', function() {
  var service = {};
  service.astronaut = {};
  service.setRocket = function(rocket) {
    this.rocket = rocket;
  };
  service.setRockets = function(rockets) {
    this.rockets = rockets;
  };
  service.setPeriods = function(periods) {
    this.periods = periods;
  };
  service.setStartDate = function(date) {
    this.startDate = date;
  };
  service.setStartHour = function(hour) {
    this.startHour = hour;
  };
  service.setAvailableStartHours = function(hours) {
    this.availableStartHours = hours;
  };
  service.setTravel = function(travel) {
    this.travel = travel;
  };
  service.setPeriodType = function(periodType) {
    this.periodType = periodType;
  };
  service.setDuration = function(duration) {
    this.duration = duration;
  };

  return service;
});



nixgonspace.factory('settings', function() {
  var service = {};
  service.astronaut = {};
  service.OPENING_TIME = 9;
  service.CLOSING_TIME = 22;
  service.BUSINESS_HOUR = service.CLOSING_TIME - service.OPENING_TIME;

  return service;
});


nixgonspace.controller('AppCtrl', function($cookieStore, $document, $filter, $http, $interval, $location, $q, $rootScope, $route, $routeParams, $scope, $timeout, $window, $mdDialog, $mdSidenav, $mdToast, users, booking, settings) {
  var OPENING_TIME = settings.OPENING_TIME;
  var CLOSING_TIME = settings.CLOSING_TIME;
  var BUSINESS_HOUR = settings.BUSINESS_HOUR;

  $scope.start = moment().startOf('day').format('YYYY-MM-DD');
  var CLIENT_JS_URL = 'https://apis.google.com/js/client.js';
  var loadScript = function(src) {
    var deferred = $q.defer();
    var script = $document[0].createElement('script');
    script.onload = function(value) {
      $timeout(function() {
        deferred.resolve(value);
      });
    };
    script.onerror = function(reason) {
      $timeout(function() {
        deferred.reject(reason);
      });
    };
    script.src = src;
    $document[0].body.appendChild(script);
    return deferred.promise;
  };

  var loadClientJs = function(callback) {
    loadScript(CLIENT_JS_URL).then(function() {
      var loaded = function(callback) {
        if (gapi.client != undefined) {
          callback();
          $interval.cancel(doCheck);
        }
      };
      loaded(callback);
      var doCheck = $interval(function() {
        loaded(callback);
      }, 10);
    });
  };

  loadClientJs(function() {
    gapi.auth.init(function() {
      $scope.loadSpaceApi();
    });
  });


  $scope.loadSpaceApi = function() {
    gapi.client.load('nixgonspace', 'v1', function() {
      // $scope.isBackendReady = true;

      $rootScope.$broadcast('listRockets');
    }, host + '/_ah/api');
  };
  $scope.showSimpleToast = function(message) {
    $mdToast.show(
        $mdToast.simple()
            .content(message)
            .position("top left")
            .parent(angular.element(document.body))
    );
  };


  $scope.arrows = {
    year: {
      left: 'img/icons/white_arrow_left.svg',
      right: 'img/icons/white_arrow_right.svg'
    },
    month: {
      left: 'img/icons/grey_arrow_left.svg',
      right: 'img/icons/grey_arrow_right.svg'
    }
  };
  $scope.header = {
    monday: 'Mon',
    tuesday: 'Tue',
    wednesday: 'Wed',
    thursday: 'Thu',
    friday: 'Fri',
    saturday: 'Sat',
    sunday: 'Sun',
  };

  $scope.menus = [
    {title: 'Store', href: '/store', icon: 'store'},
    // {title: 'Cart', href: '/cart', icon: 'shopping_cart'},
    {title: 'Order History', href: '/orders', icon: 'history'},
    {title: 'Event List', href: '/periods', icon: 'event'},

    {title: 'Profile', href: '/user', icon: 'account_circle'},
    {title: 'NIXGON SPACE', href: '/nixgonspace', icon: 'info'},
    {title: 'Sign Out', href: '/signout', icon: 'exit_to_app'}
  ];
  $scope.goTo = function(path) {
    if($scope.user.email === null) {
      $location.url('/');
      $scope.showSimpleToast('로그인 해 주시기 바랍니다.');
    } else if (path==='/nixgonspace') {
      window.open('http://space.nixgon.com','_blank');
    } else {
      if (path === '/signout') {
        $scope.signOut();
        return;
      }
      $location.url(path);
    }
    $mdSidenav('left').toggle();
  };
  $scope.openSidenav = function() {
    $mdSidenav('left').toggle();
  };

  $scope.signInCheck = function () {
    if($scope.user.email === null) {
      $location.url('/');
      $scope.showSimpleToast('로그인 해 주시기 바랍니다.');
    }

  }

  $scope.selectRocket = function(r) {
    booking.setRocket(r);
    booking.setStartDate($scope.start);
    console.log($scope.start);
    if(!r.disable) {
      $scope.rocket = angular.copy(r);
      $location.path('/store/' + r.id);
    } else {
      $scope.showSimpleToast('현 시간에 해당 상품을 예약 하실 수 없습니다.');
    }
  };

  $scope.user =
          $cookieStore.get('userData') === undefined ? {
    password: null,
    email: null,
    familyName: null,
    givenName: null,
    mobileNumber: null,
    verified: null,
    created: null,
    point: null,
    level: null,
    notes: null,
    picture: '/img/icons/ic_account_circle_24px.svg'
  } : $cookieStore.get('userData');
  users.setUser($scope.user);

  $scope.signedIn = $cookieStore.get('userData') === undefined ? false : true;
  var nowTime = new Date();
  var lastLoginTime = new Date($cookieStore.get('LastloginDateTime'));
  $cookieStore.put('LastloginDateTime', nowTime);


  if( nowTime.getTime() - lastLoginTime.getTime() > 10*60*1000) {
    $scope.signedIn = false;
  }

  $scope.goToAdmin = function() {
    window.location.href = '/admin';
  };
  $scope.isNixgon = $cookieStore.get('userData') === undefined ? false : true;
  $scope.login = {
    email: null,
    password: null
  }
  $scope.goToSignIn = function() {
    $location.url('/signin');
  };

  $scope.goToSignUp = function() {
    $location.url('/signup');
  };
  $scope.signIn = function() {
    gapi.client.nixgonspace.auth.login({
      email: $scope.login.email,
      password: md5($scope.login.password)
    }).then(function(response) {
      console.log(response);
      $scope.signedIn = true;
      gapi.client.nixgonspace.astronauts.get({
        astronautId: $scope.login.email
      }).execute(function(response) {
        $scope.user = response.result;
//          var lastLoginTime = new Date($cookieStore.get('LastloginDateTime'));
        $cookieStore.put('userData', response.result);
        $cookieStore.put('LastloginDateTime',new Date());

        if ($scope.user.email.indexOf('nixgon.com', $scope.user.email.length - 'nixgon.com'.length) !== -1) {
          $scope.isNixgon = true;
        }
        users.setUser($scope.user);

        $rootScope.$broadcast('listRockets');
        $location.url('/');
        $scope.$apply();
      });

    }, function(reason) {
      $scope.showSimpleToast('비밀번호가 틀렸습니다.');
      $location.url('/signin');
      $scope.$apply();
    });
  };

  $scope.reloadRoute = function() {
    // $route.reload();
    // $window.location.reload();
  };
  $scope.signInAnother = function() {
    gapi.auth.signOut();
    $scope.signedIn = false;
    $cookieStore.put('userData',undefined);
    $scope.user ={
      password: null,
      email: null,
      familyName: null,
      givenName: null,
      mobileNumber: null,
      verified: null,
      created: null,
      point: null,
      level: null,
      notes: null,
      picture: '/img/icons/ic_account_circle_24px.svg'
    };
    window.location.href = redirect;
  };

  $scope.signOut = function() {
//    gapi.auth.signOut();
    $scope.signedIn = false;
    $cookieStore.remove('userData');
    $scope.user = {
      password: null,
      email: null,
      familyName: null,
      givenName: null,
      mobileNumber: null,
      verified: null,
      created: null,
      point: null,
      level: null,
      notes: null,
      picture: '/img/icons/ic_account_circle_24px.svg'
    };
  };

  $scope.isVerified = false;
  $scope.inputCode;
  $scope.rightCode;
  $scope.requestCount = 0;
  $scope.secs = 60;
  $scope.isRequestDisabled = false;
  $scope.requestCode = function() {
    if ($scope.user.mobileNumber === null) {
      $scope.showSimpleToast('Please insert your phone number.');
      return;
    }

    $scope.requestCount++;
    if ($scope.requestCount == 2) {
      $scope.isRequestDisabled = true;
    }

    $scope.secs = 60;
    var stop = $interval(function() {
      if ($scope.requestCount == 2) {
        $interval.cancel(stop);
        $scope.requestCount = 0;
      }

      if ($scope.secs === 1) {
        $interval.cancel(stop);
        $scope.isDisabled = false;
      }
      $scope.secs--;
    }, 1000);

    $scope.rightCode = Math.floor(Math.random() * 8999 + 1000);

    $scope.isRequested = true;
    gapi.client.nixgonspace.sms.insert({
      to: '+82' + $scope.user.mobileNumber,
      body: '[NIXGON SPACE] 인증코드는 ' + $scope.rightCode + ' 입니다.'
    }).execute(function(response) {
      console.log(response);
    });
  };

  $scope.isDisabled = false;
  $scope.verify = function(inputCode) {
    if (inputCode == undefined) {
      $scope.showSimpleToast('인증코드를 입력해주세요.');
      return;
    }
    if (inputCode == $scope.rightCode) {
      $scope.isDisabled = true;
      $scope.isVerified = true;
      $scope.showSignUpButton = true;
    }
  };

  $scope.isRequested = false;
  $scope.saveChanges = function() {
    gapi.client.nixgonspace.astronauts.update({
      astronautId: $scope.user.email,
      resource : $scope.user
    }).execute(function(response) {
      $scope.isDisabled = false;
      $scope.isRequested = false;
      $scope.isVerified = false;
      $scope.inputCode = '';
      $scope.secs = 60;
    });
  };

  var ASTRONAUT_LEVEL_MEMBER = 'MEMBER';


  $scope.retype = {
    email: null,
    password: null
  };
  $scope.signUp = function(user) {

    if ($scope.user.email !== $scope.retype.email) {
      $scope.showSimpleToast('이메일이 일치하지 않습니다.');
      return;
    }
    if ($scope.user.password !== $scope.retype.password) {
      $scope.showSimpleToast('비밀번호가 일치하지 않습니다.');
      return;
    }
    if (!$scope.isVerified) {
      $scope.showSimpleToast('휴대폰 번호를 입력해주세요.');
      return;
    }

    var astronaut = {
      password: md5(user.password),
      email: user.email,
      familyName: user.familyName,
      givenName: user.givenName,
      mobileNumber: user.mobileNumber,
      verified: false,
      level: ASTRONAUT_LEVEL_MEMBER,
      picture: user.picture
    };
    gapi.client.nixgonspace.astronauts.insert(
        astronaut
    ).execute(function(response) {
          $scope.showSimpleToast('회원가입이 완료되었습니다.');
          $scope.signIn();
        });
  };

  $scope.isRocketsReady = false;

  $scope.start;
  function DatePicker($scope, $mdDialog) {
    $scope.startDate = new Date();
    $scope.close = function() {
      $mdDialog.cancel();
    }

  }
  $scope.showDatePicker = function() {
    $mdDialog.show({
      controller: DatePicker,
      templateUrl: 'partials/date-picker-dialog.html'
    });
  };

  $scope.startDisplay;

  $scope.rocketFilter = function (rockets) {
    if (rockets != null) {

      var now = new Date();
      var nowType = '';
      // 시간을 나누어 해당 시간에는 이 타입이 활성화 되지 않음
      // ex> 오전 중에는 EVENING 타입을 판매하지 않음음
      if (now.getHours() >= 9 && now.getHours() < 17) {
        nowType = 'EVENING';
      } else if (now.getHours() >= 17 && now.getHours() <= 22) {
        nowType = 'DAILY';
      } else {
        nowType = "DAY";
      }

      angular.forEach(rockets, function (rocket) {
//        console.log(rocket,nowType , rocket.type)
        if (rocket.type === nowType ) { rocket.disable = true; }
        else { rocket.disable = false; }
      });
      return rockets;
    }
  };



  $scope.showRocketDetail = function(rocket) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'partials/product-detail-dialog.html',
      locals: {
        rocket: rocket
      }
    });
  };

  function DialogController($scope, $mdDialog, rocket) {
    $scope.rocket = rocket;
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
  }

  var ROCKET_TYPE_MONTHLY = 'MONTHLY';
  var ROCKET_TYPE_DAILY = 'DAILY';
  var ROCKET_TYPE_EVENING = 'EVENING';
  var ROCKET_TYPE_HOURLY = 'HOURLY';

  $scope.ROCKET_TYPE_LIST = [ROCKET_TYPE_HOURLY,ROCKET_TYPE_EVENING,ROCKET_TYPE_DAILY,ROCKET_TYPE_MONTHLY];


  $scope.periods = [];
  $scope.periodType = {
    HOURLY  : {
      timeType : "days" ,  colType:"hours",  timeselectable : true,
      startTime: OPENING_TIME, endTime:CLOSING_TIME, minTime:1,
      duration : BUSINESS_HOUR, minDuration : 1, maxDuration : BUSINESS_HOUR,
      durationString:"Hour(s)"},

    EVENING : {timeType : "days" ,  colType:"hours",  timeselectable : false, startTime: 17,           endTime:CLOSING_TIME, minTime:5,  duration : 5,  minDuration : 1, maxDuration : 30, durationString:"Day(s)"},
    DAILY   : {timeType : "days" ,  colType:"days",   timeselectable : false, startTime: OPENING_TIME, endTime:CLOSING_TIME, minTime:13, duration : 13,  minDuration : 1, maxDuration : 30, durationString:"Day(s)"},
    MONTHLY : {timeType : "months", colType:"months", timeselectable : false, startTime: OPENING_TIME, endTime:CLOSING_TIME, minTime:13, duration : 13,   minDuration : 1, maxDuration :  3, durationString:"Month(s)"}
  };
  booking.setPeriodType($scope.periodType);



  $scope.space = {};
  $scope.selectSpace = function(space) {
//		$scope.start = moment().startOf('day');
    $scope.space = space;
    if ($scope.space.category === 'WORK') {
      $scope.startHour = OPENING_TIME;
      moment($scope.start).add($scope.startHour, 'hours');
      $location.url('/store/' + $routeParams.rocketId + '/' + $scope.space.id + '/' + $scope.startHour);
    } else {
      $location.url('/store/' + $routeParams.rocketId + '/' + $scope.space.id);
    }
  };

  $scope.availableStartHours = [];



  $scope.startHour;
  $scope.availableDurationsEachTime = [];


  function compareDate(period1, period2) {
    var date1 = moment(period1.start).startOf('day');
    var date2 = moment(period2.start).startOf('day');
    return date1.isBefore(date2);
  }
  $scope.durationUnit = '';
  $scope.availableDuration = [];


  $scope.duration;
  $scope.travel = {};
  $scope.travel.usedPoint = 0;
  $scope.travel.payment = {"isOnline":undefined,"method":'CARD'};



  $scope.proceedToCheckout = function(travel) {
    if ($scope.user.email === null) {
      $scope.showSimpleToast('로그인 후 예약할 수 있습니다.');
      return;
    }
    if ($scope.rocket.type === 'EVENING') {
      var now = new Date();
      if (now.getHours() < 17) {
        $scope.showSimpleToast('이브닝 패스 예약이 불가능합니다.');
        return;
      }
    }
    if ($scope.rocket.type === 'DAILY') {
      var now = new Date();
      if (now.getHours() >= 17) {
        $scope.showSimpleToast('1일 패스 예약이 불가능합니다.');
        return;
      }
    }

    booking.setTravel(travel);
    $scope.travel = travel;
    $location.url('/checkout');
  };

  var eventInfo = {
    title: null,
    description: null,
    attendance: 0,
    organizer: null,
    contacts: null
  };
  $scope.eventInfo = eventInfo;


  $scope.isTravelsReady = false;
  $scope.travels = [];
  $scope.listTravels = function() {
    gapi.client.nixgonspace.travels.list({
      astronautId: $scope.user.email
    }).execute(function(response) {
      console.log(response)
      $scope.isTravelsReady = true;
      $scope.travels = response.items;
      angular.forEach($scope.travels, function(travel) {
        travel.date = moment(travel.date).format();
      });
      console.log($scope.travels);
      $scope.$apply();
    });
  };
  $scope.myPeriods = [];
  $scope.listPeriods = function() {
    gapi.client.nixgonspace.periods.list({
      astronautId: $scope.user.email
    }).execute(function(response) {
      $scope.myPeriods = response.items;
      console.log($scope.myPeriods)
    });
  };

  var MAXIMUM_AVAILABLE_DAY = 30;
  $scope.$watch('start', function(newVal, oldVal, scope) {
    var todayDate = moment().format('YYYY-MM-DD');
    var diff = moment(newVal).diff(todayDate, 'day');
    if (diff<0) {
      scope.showSimpleToast("오늘 이전 날짜는 예약하실 수 없습니다.");
      scope.start = todayDate;
    } else if ( diff > MAXIMUM_AVAILABLE_DAY ) {
      scope.showSimpleToast("닉스곤 스페이스의 최대가능 예약일은 " + MAXIMUM_AVAILABLE_DAY+ "일 입니다. 이후 날짜에 대한 예약은 불가능합니다. ");
      scope.start = todayDate;
    }
  });

  $scope.showTravelDetail = function(ev, travel) {
    $mdDialog.show({
      controller: DetailCtrl,
      templateUrl: 'partials/order-detail-dialog.html',
      targetEvent: ev,
      locals: {
        travel: travel
      }
    });
  };

  $scope.showMapDialog = function() {
    var travel = null;
    $mdDialog.show({
      controller: DetailCtrl,
      templateUrl: 'partials/map-dialog.html',
//			targetEvent: ev,
      locals: {
        travel: travel
      }
    });
  };

  function DetailCtrl($scope, $mdDialog, travel) {
    $scope.travel = travel;
    $scope.closeDialog = function() {
      $mdDialog.hide();
    };
  }

  $scope.cancelOrder = function(travel) {
    travel.status = 'USER_CANCELED';
    console.log(travel);
    gapi.client.nixgonspace.travels.update({
      travelId: travel.id,
      resource: travel
    }).execute(function(response) {

    });
  };


  $scope.rocket;


});


nixgonspace.controller('storeCtrl', function($cookieStore, $document, $filter, $http, $interval, $location, $q, $rootScope, $route, $routeParams, $scope, $timeout, $window, $mdDialog, $mdSidenav, $mdToast, users, booking){
  $scope.rockets = [];
  $scope.isRocketsReady=false;


  $scope.$on('listRockets', function() {
    $scope.listRockets();
  });
  $scope.listRockets = function() {
    if ($scope.rockets.length === 0) {
      $scope.start = moment().startOf('day').format('YYYY-MM-DD');
      var rockets = null;
      if (gapi != undefined) {
        gapi.client.nixgonspace.rockets.list().execute(function(response) {
          $scope.$apply( function () {
            $scope.rockets = $scope.rocketFilter(response.items);
            booking.setRockets($scope.rockets);
            $scope.isRocketsReady = true;
          });
        });
      } else {
        $http.get('/json/rockets.json').success(function(data) {
          $scope.isRocketsReady = true;
          $scope.$apply( function () {
            $scope.rockets = $scope.rocketFilter(data.rockets);
            booking.setRockets($scope.rockets);
            $scope.isRocketsReady = true;
          });
        });
      }
    }
  };
});


nixgonspace.controller('durationCtrl', function($cookieStore, $document, $filter, $http, $interval, $location, $q, $rootScope, $route, $routeParams, $scope, $timeout, $window, $mdDialog, $mdSidenav, $mdToast, users, booking, settings) {
  var OPENING_TIME = settings.OPENING_TIME;
  var CLOSING_TIME = settings.CLOSING_TIME;
  var BUSINESS_HOUR = settings.BUSINESS_HOUR;
  $scope.start = booking.startDate;

  $scope.periods = booking.periods;
  $scope.availableStartHours = booking.availableStartHours === undefined ? [] : booking.availableStartHours;

  $scope.listAvailableStartHours = function() {

    var availableStartHours = [];
    var startTime =
        ( moment($scope.start).isSame(moment(),'day') &&  Number(moment().format('HH')) > OPENING_TIME) ?
            Number(moment().format('HH')) : OPENING_TIME;

    for (var i = startTime; i < CLOSING_TIME; i++) {
      availableStartHours.push(i);
    }
    angular.forEach($scope.periods[$scope.space.type+$scope.space.number], function (dateTime) {
      for(var i = 0; i<dateTime.duration; i++) {
        availableStartHours.splice(availableStartHours.indexOf(Number(dateTime.periodTime)+i), 1);
      }
    });

    $scope.availableStartHours = angular.copy(availableStartHours);
    booking.setAvailableStartHours($scope.availableStartHours);

  };

  $scope.selectStartHour = function(startHour) {
    $scope.startHour = startHour;
    booking.setStartHour($scope.startHour);
    booking.setStartDate(moment($scope.start).add($scope.startHour, 'hours'));
    $location.url('/store/' + $scope.rocket.id + '/' + $scope.space.id + '/' + startHour);
  };



  $scope.listAvailableDuration = function() {
    $scope.availableDuration = [];

    if ($scope.rocket.type === 'HOURLY') { $scope.startHour = booking.startHour;}

    var periodType = $scope.space.type+$scope.space.number;
    var maxDuration = 0;

    var period = $scope.periods[periodType] ;


    if (period === undefined) {
      if ($scope.rocket.type === 'HOURLY') {
        maxDuration = $scope.availableStartHours.length - $scope.availableStartHours.indexOf($scope.startHour);
      } else {
        maxDuration = $scope.periodType[$scope.rocket.type].maxDuration;
      }
    } else if ($scope.rocket.type === 'EVENING' || $scope.rocket.type === 'DAILY' ) {
      $filter('orderBy')(period, 'periodDate');
      maxDuration = moment(period[0].periodDate).diff(moment($scope.start), 'days');
    } else if ($scope.rocket.type === 'MONTHLY') {
      $filter('orderBy')(period, 'periodDate');
      maxDuration = moment(period[0].periodDate).diff(moment($scope.start), 'months');
    } else if ($scope.rocket.type === 'HOURLY') {
      var startindex = $scope.availableStartHours.indexOf($scope.startHour);
      console.log(period, $scope.startHour, startindex)
      for (var i = startindex; i<$scope.availableStartHours.length-1 ;i++) {
        if (i !== $scope.availableStartHours.length) {
          if ($scope.availableStartHours[i] === $scope.availableStartHours[i+1] -1) {
            maxDuration ++;
          }
        }
      }
    }
    for (var i=1;i<=maxDuration;i++) {
      $scope.availableDuration.push(i);
      $scope.durationUnit = $scope.periodType[$scope.rocket.type].durationString;
    }
  };

  $scope.selectDuration = function(duration) {
    $scope.duration = duration;
    booking.setDuration(duration);
    var travel = {
      astronautId: $scope.user.email,
      period: {
        space: $scope.space,
        start: moment($scope.start).format(),
        duration: $scope.duration
      },
      rocketId: $scope.rocket.id,
      status: null,
      payment: {
        isOnline: false,
        method: "CARD"
      },
      usedPoint: 0,
      price: $scope.rocket.price * $scope.duration
    };
//    gapi.client.nixgonspace.travels.list({
//      timeMin: moment($scope.start).startOf('day').format(),
//      timeMax: moment($scope.start).add(1, 'days').startOf('day').format()
//    }).execute(function(response) {
//      $scope.travels = response.items;
//      $scope.$apply();
//    }); ??????

    $scope.proceedToCheckout(travel);

  };

});
nixgonspace.controller('checkOutCtrl', function($cookieStore, $document, $filter, $http, $interval, $location, $q, $rootScope, $route, $routeParams, $scope, $timeout, $window, $mdDialog, $mdSidenav, $mdToast, users, booking, settings) {

  $scope.travel = booking.travel;

  $scope.users = users.user;
  console.log($scope.travel, users.user);


    $scope.PData = {
      "CST_MID" : "nixgon",
      "platform" : "test",
      "LGD_BUYER" : users.user.familyName + users.user.givenName,
      "LGD_PRODUCTINFO" :$scope.travel.period.space.type+$scope.travel.period.space.number,
      "LGD_AMOUNT" : $scope.travel.price,
      "LGD_BUYEREMAIL" : users.user.email,
      "LGD_OID" : "test_1234567890020",
      "LGD_TIMESTAMP" :"1234567890",
      "LGD_MERTKEY" : "ce0946fca06a6c97743fbdc568d05868",
      "LGD_MID" : "tnixgon",
      "LGD_CUSTOM_SKIN" : "red",
      "LGD_WINDOW_VER" : "2.5",
      "LGD_BUYERID" : users.user.email,
      "LGD_NOTEURL" : "http://nixgonspace.appspot.com/#/paymentcomplete",
      "POINT_PAY" : 0,
      "TOTAL_PAYMENT" : $scope.travel.price,
      "USER_MOBILE" : users.user.mobileNumber,
      "ONLINE":true
    };

  $scope.PData.LGD_HASHDATA = md5(
          $scope.PData.LGD_MID +
          $scope.PData.LGD_OID +
          $scope.PData.LGD_AMOUNT +
          $scope.PData.LGD_TIMESTAMP + $scope.PData.LGD_MERTKEY);





  $scope.placeOrder = function(travel) {
    var maxUseAblePoint = $scope.travel.price < $scope.user.point ? $scope.travel.price : $scope.user.point;

    if(isNaN($scope.travel.usedPoint) == true) {
      $scope.showSimpleToast("사용 포인트에 잘못된 금액이 들어갔습니다. 확인 부탁드립니다.");
    } else if (maxUseAblePoint < $scope.travel.usedPoint) {
      $scope.showSimpleToast("최대 사용 가능한 포인트를 초과하였습니다.");
      $scope.travel.usedPoint = maxUseAblePoint;
    } else if ( 0 > $scope.travel.usedPoint) {
      $scope.showSimpleToast("사용 포인트에 음수가 들어갈 수 없습니다.");

    } else if ($scope.travel.usedPoint % 100 !== 0) {
      $scope.showSimpleToast("포인트는 100원 단위로 사용 가능합니다.");
    } else {
      if ($scope.travel.payment.isOnline === undefined) {
        $scope.showSimpleToast("결제 타입을 선택해 주십시오");
      } else if ($scope.travel.payment.method === undefined) {
        $scope.showSimpleToast("결제 방법을 선택해 주십시오");
      } else {
        $location.url('/payrequest');
      }
    }
  };

  $scope.payRequest = function(travel) {
//    $scope.eventInfo = eventInfo;
    $scope.travel.status = "ORDERED";
//    $scope.travel.eventInfo = $scope.eventInfo;
    $scope.periodType = booking.periodType;
    console.log(booking)
    var travels = {
      "astronautId": $scope.travel.astronautId,
      "rocketId": $scope.rocket.id,
      "quantity": booking.duration,
      "status": "ORDERED",
      "payment": $scope.travel.payment,
      "price": $scope.travel.price,
      "usedPoint": $scope.travel.usedPoint
    };
    var duration = $scope.rocket.type !=='HOURLY' ? $scope.periodType[$scope.rocket.type].minTime
        : $scope.periodType[$scope.rocket.type].minTime * booking.duration;
    var startTime = $scope.rocket.type !=='HOURLY' ? $scope.periodType[$scope.rocket.type].startTime : booking.startHour;
    var endTime = $scope.rocket.type !=='HOURLY' ? $scope.periodType[$scope.rocket.type].endTime
        : startTime + booking.duration;

//    console.log(booking)
//    console.log(startTime)
//    console.log(moment().startOf('day'))
    var period  = {
      "astronautId": $scope.travel.astronautId,
      "space": $scope.travel.period.space,
      "start": moment().startOf('day').set('hour',startTime).format(),
      "end": moment().startOf('day').set('hour',endTime).format(),
      "duration": duration,
      "status" : "PREEMPTION"
    };

    gapi.client.nixgonspace.orders.insert({
      period: period,
      travel: travels
    }).execute(function (response) {
      console.log(response);
      $location.url('/');
      $scope.showSimpleToast("예약이 완료되었습니다.");
      $scope.$apply();
    });
  };
});

nixgonspace.controller('rocketCtrl', function($cookieStore, $document, $filter, $http, $interval, $location, $q, $rootScope, $route, $routeParams, $scope, $timeout, $window, $mdDialog, $mdSidenav, $mdToast, users, booking, settings) {
  var OPENING_TIME = settings.OPENING_TIME;
  var CLOSING_TIME = settings.CLOSING_TIME;
  var BUSINESS_HOUR = settings.BUSINESS_HOUR;

  $scope.rockets =booking.rockets;
  $scope.rocket =booking.rocket;
  $scope.isRocketsReady=false;
  $scope.isSpacesReady = false;
  $scope.spaces=[];
  $scope.tempSpaces;


  $scope.listSpacesFromRocket = function() {
    angular.forEach($scope.rockets, function(rocket) {
      if (rocket.id === $routeParams.rocketId) {
        $scope.rocket = rocket;
        $scope.listSpaces();
      }
    });
  };


  $scope.listSpaces = function() {
    gapi.client.nixgonspace.spaces.list({
      type: $scope.rocket.spaceType
    }).execute(function(response) {
      $scope.tempSpaces = $filter('orderBy')(response.items, 'number');
      $scope.$apply(function(){
        $scope.isSpacesReady = false;
        $scope.spliceReservedSpace();
      });
    });
  };

  $scope.spliceReservedSpace = function() {
    var periods = null;
    var nowDate = moment($scope.start).format('YYYY-MM-DD');
    var nowTime = moment($scope.start).format('HH:mm');
    var periodType = $scope.periodType[$scope.rocket.type];
    var spacePeriodList = [];
    var typeNumber = "";
    var availableStartHours = [];
    var spaces = [];
    var startTime =
        ( moment($scope.start).isSame(moment(), 'day') && Number(moment().format('HH')) > OPENING_TIME) ?
            Number(moment().format('HH')) : OPENING_TIME;
    for (var i = startTime; i < CLOSING_TIME; i++) {
      availableStartHours.push(i);
    }
    gapi.client.nixgonspace.periods.list({
      timeMin: moment($scope.start).format(),
      timeMax: moment($scope.start).add(periodType.duration, periodType.timeType).format(),
      spaceType: $scope.rocket.spaceType
    }).execute(function(response) {
      if ($scope.rocket.spaceType === 'IS' || $scope.rocket.spaceType === 'ISS') {
        var spaceType = $scope.rocket.spaceType === 'IS' ? 'ISS' : 'IS';
        gapi.client.nixgonspace.periods.list({
          timeMin: moment($scope.start).format(),
          timeMax: moment($scope.start).add(periodType.duration, periodType.timeType).format(),
          spaceType: spaceType
        }).execute(function(elseResponse) {
          if ($scope.rocket.spaceType === 'IS') {
            angular.forEach(response.items, function (period) {
              period.date = moment(period.start).format('YYYY-MM-DD');
              period.time = moment(period.start).format('HH');
              if (spacePeriodList[period.space.type + period.space.number] === undefined) {
                spacePeriodList[period.space.type + period.space.number] = [];
              }
              spacePeriodList[period.space.type + period.space.number].push({
                periodDate: period.date,
                periodTime: period.time,
                duration: period.duration
              });
            });

            angular.forEach(elseResponse.items, function (period) {
              period.date = moment(period.start).format('YYYY-MM-DD');
              period.time = moment(period.start).format('HH');
              if (spacePeriodList.IS1 === undefined) {
                spacePeriodList.IS1 = [];
              }
              spacePeriodList.IS1.push({
                periodDate: period.date,
                periodTime: period.time,
                duration: period.duration
              });
            });

          } else if ($scope.rocket.spaceType === 'ISS') {
            angular.forEach(response.items, function (period) {
              period.date = moment(period.start).format('YYYY-MM-DD');
              period.time = moment(period.start).format('HH');
              if (spacePeriodList[period.space.type + period.space.number] === undefined) {
                spacePeriodList[period.space.type + period.space.number] = [];
              }
              spacePeriodList[period.space.type + period.space.number].push({
                periodDate: period.date,
                periodTime: period.time,
                duration: period.duration
              });
            });


            angular.forEach(elseResponse.items, function (period) {
              period.date = moment(period.start).format('YYYY-MM-DD');
              period.time = moment(period.start).format('HH');
              for (var i = 1; i<=6; i++){
                var spType = 'ISS' + i;
                if (spacePeriodList[spType] === undefined) {
                  spacePeriodList[spType] = [];
                }
                spacePeriodList[spType].push({
                  periodDate: period.date,
                  periodTime: period.time,
                  duration: period.duration
                });
              }
            });
          }
        });
        angular.forEach($scope.tempSpaces, function (space) {
          var spaceNumber = space.type + space.number;
          var periods = spacePeriodList[spaceNumber];
          $filter('orderBy')(periods, 'periodDate');
          if (periods === undefined) {
            spaces.push(space);
          } else if ($scope.rocket.type === 'MONTHLY') {
            var durationDate = moment($scope.start).add(1, 'months').format('YYYY-MM-DD');
            var periodDate = periods[0].periodDate;
            if (moment(periodDate).diff(durationDate, 'day') > 0) {
              spaces.push(space);
            }
          } else {
            if (periods[0].periodDate === moment($scope.start).format("YYYY-MM-DD")) {
              availableStartHours.splice(availableStartHours.indexOf(Number(periods[0].periodTime)), periods[0].duration);
            }
            console.log(spaceNumber, periods, availableStartHours, periodType.startTime, periodType.endTime)
            if (availableStartHours.filter(function (x) {
              console.log(x)
              return ( x >= periodType.startTime && x <= periodType.endTime);
            }).length >= periodType.minTime) {
              spaces.push(space);
            }
          }
          $scope.spaces = spaces;
        });
        $scope.periods = spacePeriodList;
        booking.setPeriods($scope.periods);
        $scope.$apply(function () {
          $scope.isSpacesReady = true;
        });
      } else {
        angular.forEach(response.items, function (period) {
          console.log(period)
          period.date = moment(period.start).format('YYYY-MM-DD');
          period.time = moment(period.start).format('HH');
          if (spacePeriodList[period.space.type + period.space.number] === undefined) {
            spacePeriodList[period.space.type + period.space.number] = [];
          }
          spacePeriodList[period.space.type + period.space.number].push({
            periodDate: period.date,
            periodTime: period.time,
            duration: period.duration
          });
        });
        angular.forEach($scope.tempSpaces, function (space) {
          var spaceNumber = space.type + space.number;
          var periods = spacePeriodList[spaceNumber];
          $filter('orderBy')(periods, 'periodDate');
          if (periods === undefined) {
            spaces.push(space);
          } else if ($scope.rocket.type === 'MONTHLY') {
            var durationDate = moment($scope.start).add(1, 'months').format('YYYY-MM-DD');
            var periodDate = periods[0].periodDate;
            if (moment(periodDate).diff(durationDate, 'day') > 0) {
              spaces.push(space);
            }
          } else {
            if (periods[0].periodDate === moment($scope.start).format("YYYY-MM-DD")) {
              availableStartHours.splice(availableStartHours.indexOf(Number(periods[0].periodTime)), periods[0].duration);
            }
            if (availableStartHours.filter(function (x) {
              return ( x >= periodType.startTime && x <= periodType.endTime);
            }).length >= periodType.minTime) {
              spaces.push(space);
            }
          }
          $scope.spaces = spaces;
        });
        $scope.periods = spacePeriodList;

        booking.setPeriods($scope.periods);

        $scope.$apply(function () {
          $scope.isSpacesReady = true;
        });
      }
    });
  };
});

nixgonspace.directive('ngEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (event.which === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
});