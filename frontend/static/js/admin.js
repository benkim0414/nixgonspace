function init() {
  window.init();
}

var isDebug = true;

//var isDebug = false;
var host = 'https://nixgonspace.appspot.com';
if (isDebug) {
  host = 'http://localhost:8080';
}


var nixgonspace = angular.module('nixgonspace.app', ['ngRoute', 'ngMaterial', 'pascalprecht.translate', 'ngMessages', 'ngCookies', 'ngMdIcons']);

nixgonspace.config(['$translateProvider', function ($translateProvider) {
  // Simply register translation table as object hash
  $translateProvider.useStaticFilesLoader({
    prefix: 'json/local-',
    suffix: '.json'
  });

  $translateProvider.use('KR');
}]);


nixgonspace.config(function($routeProvider) {
  $routeProvider.when('/order', {
    controller: 'AdminCtrl',
    templateUrl: 'admin/OrderManagement.html'
  }).when('/members', {
    controller: 'AdminCtrl',
    templateUrl: 'admin/MemberManagement.html'
  }).when('/checkin', {
    controller: 'AdminCtrl',
    templateUrl: 'admin/CheckIn.html'
  }).when('/checkout', {
    controller: 'AdminCtrl',
    templateUrl: '/admin/CheckOut.html'
  }).when('/rfidupdate', {
    controller: 'AdminCtrl',
    templateUrl: '/admin/rfids.html'
  }).when('/payment', {
    controller: 'AdminCtrl',
    templateUrl: '/admin/payment/sample.html'
  }).when('/inuse', {
    controller: 'AdminCtrl',
    templateUrl: '/admin/inUse.html'
  }).when('/payreq', {
    controller: 'AdminCtrl',
    templateUrl: '/admin/payment/payreq.html'
  }).otherwise({
    redirectTo: '/order'
  });
});


nixgonspace.factory('userService', function() {
  var service = {};
  service.userinfo = {};

  service.extendUserinfo = function(userinfo) {
    angular.extend(this.userinfo, userinfo);
  };

  return service;
});

nixgonspace.factory('paymentService', function() {
  var service = {};
  service.paymentinfo = {};

  service.extendUserinfo = function(paymentinfo) {
    angular.extend(this.paymentinfo, paymentinfo);
  };

  return service;
});

nixgonspace.controller('HomeCtrl', function($scope, $window, $location, $q, $mdSidenav, userService, $translate, $mdDialog, $document, $timeout, $interval, $cookieStore, $rootScope, $http, paymentService) {
  var signInCheck = false;
  var tempStringArray=[];

  $window.init = function() {
    loadClientJs(function() {
      gapi.auth.init(function() {

        userService.extendUserinfo($cookieStore.get('userData'));
        $scope.$apply($scope.loadSpaceApi);
      });
    });
  };



  var CLIENT_JS_URL = 'https://apis.google.com/js/client.js';

  var loadScript = function(src) {
    var deferred = $q.defer();
    var script = $document[0].createElement('script');
    script.onload = function(value) {
      $timeout(function() {
        deferred.resolve(value);
      });
    };
    script.onerror = function(reason) {
      $timeout(function() {
        deferred.reject(reason);
      });
    };
    script.src = src;
    $document[0].body.appendChild(script);
    return deferred.promise;
  };

  var loadClientJs = function(callback) {
    loadScript(CLIENT_JS_URL).then(function() {
      var loaded = function(callback) {
        if (gapi.client != undefined) {
          callback();
          $interval.cancel(doCheck);
        }
      };
      loaded(callback);
      var doCheck = $interval(function() {
        loaded(callback);
      }, 10);
    });
  };

  $scope.signOut = function() {
    gapi.auth.signOut();
    $scope.signedIn = false;
    document.location.href = "https://www.google.com/accounts/Logout?" +
        "continue=https://appengine.google.com/_ah/logout?" +
        "continue=https://nixgonspace.appspot.com";
  };


  $scope.userinfo = userService.userinfo;



  $scope.isBackendReady = false;
  $scope.loadSpaceApi = function() {

    gapi.client.load('nixgonspace', 'v1', function() {
      $scope.isBackendReady = true;
      $scope.listRockets();
    }, host + '/_ah/api');
  };

  $scope.rockets = [];
  $scope.listRockets = function() {
    gapi.client.nixgonspace.rockets.list().execute(function(response) {
      $scope.rockets = response.items;
      $rootScope.$broadcast('login');
      $scope.$apply();
    });
  };


  $scope.menuLists = [
    { icon : "shopping_basket",menuName : "Order Manager",  urlString:"/admin" },
    { icon : "file_download",  menuName : "CHECK_IN_PAGE",  urlString:"/checkin" },
    { icon : "local_library",  menuName : "사용중",           urlString:"/inuse" },
//    { icon : "payment",        menuName : "payment",        urlString:"/payment" },
    { icon : "file_upload",    menuName : "CHECK_OUT_PAGE", urlString:"/checkout" },
    { icon : "contacts",       menuName : "Member Manager", urlString:"/members" },
    { icon : "credit_card",    menuName : "RFID",           urlString:"/rfidupdate" },
    { icon : "exit_to_app",    menuName : "SignOut",        urlString:"signOut" }
  ];

  $scope.urlString = function (url) {

    if (url==="signOut" ) {
      $scope.signOut();
    } else if (url==="termsOfUse") {
      window.open('http://space.nixgon.com/support/terms', '_blank');
    } else {

      $location.url(url);
    }
  };



  $scope.signIn = function() {
    var apisToLoad;
    var loadCallback = function(resp) {
      if (--apisToLoad == 0) {
        signin($scope.immediate,userAuthed);
      }
    };
    var userAuthed = function(authResult) {

      if (authResult.status.signed_in == true){
        // 구글 로그인 되어 있는 경우
        // 회원여부 확인
        gapi.client.oauth2.userinfo.get().then(function(response) {

//          if(response.result.hd ==='nixgon.com'){
            userService.extendUserinfo(response.result);
            angular.extend($scope.userinfo, userService.userinfo);
            if (!response.code) {
//              console.log(authResult);
              gapi.client.nixgonspace.astronauts.get({
                astronautId: response.result.email
              }).then(function (response) {
                // 회원인 경우
                $scope.$apply(function () {
                  userService.extendUserinfo(response.result);
                  angular.extend($scope.userinfo, userService.userinfo);
                  $scope.signedIn = true;

                  $rootScope.$broadcast('login');
                });
              }, function (reason) {
                $scope.$apply(function () {
                  // 회원이 아닌 경우
                  $location.url('/signup');
                });
              });
            }
//          } else {
//            $scope.signOut();
//          }

        });

      } else {
        // 구글 로그인 되어 있지 않는 경우
        // 로그인 페이지로 이동
        var url = 'https://nixgonspace.appspot.com';
        var scope= ['https://www.googleapis.com/auth/userinfo.email'];
        var client_id = '313370681103-546f6qpfd8qbi4k748qeh8ekokas2rem.apps.googleusercontent.com';
        var redirect =
            "https://accounts.google.com/o/oauth2/auth?" +
            "scope=" + scope.join(' ') +
            "&state=oauth2callback" +
            "&response_type=token" +
            "&client_id=" + client_id +
            "&redirect_uri="+url;
        window.location.href=redirect;
      }
    };
    apisToLoad = 2;

    var apiRoot = '/_ah/api';
    gapi.client.load('nixgonspace', 'v1', loadCallback, apiRoot);
    gapi.client.load('oauth2', 'v2', loadCallback);


    var signin = function(mode, loadCallback) {
      if(signInCheck===false){
        signInCheck=true;
        gapi.auth.authorize({
          client_id: '313370681103-546f6qpfd8qbi4k748qeh8ekokas2rem.apps.googleusercontent.com',
          scope: [
            'https://www.googleapis.com/auth/userinfo.email',
          ],
          immediate: true
        },loadCallback);
      }
    };
  };
});



nixgonspace.controller('AdminCtrl', function($scope, $http, $interval, $q, $mdDialog, $mdToast, userService, $filter, paymentService, $location) {

  $scope.userList   = {};
  $scope.originOrderList = {};
  $scope.orderList  = {};
  $scope.cardList = {};
  $scope.testPaymentData = {
    "CST_MID" : "",
    "platform" : "test",
    "LGD_BUYER" : "홍길동",
    "LGD_PRODUCTINFO" :"myLG070-인터넷전화기",
    "LGD_AMOUNT" : "50000",
    "LGD_BUYEREMAIL" :"",
    "LGD_OID" : "test_1234567890020",
    "LGD_TIMESTAMP" :"1234567890",
    "LGD_MERTKEY" : ""
  };
  $scope.paymentinfo = paymentService.paymentinfo;
  $scope.rfidCheck = "";
  $scope.errMsg = "";
  $scope.rfidChk = false;
  $scope.userInfo = userService;
  $scope.fullStatusList =
      [ "ORDERED","OFFLINE_PENDING","ONLINE_PENDING","OFFLINE_PAID","ONLINE_PAID","BEFORE_USE",
        "CHECK_IN","CHECK_OUT","USER_CANCELED","ADMIN_CANCELED","SYSTEM_CANCELED"];
  $scope.statusList =
      [ "ORDERED",  "OFFLINE_PAID", "ADMIN_CANCELED" ];
  $scope.isSpacesReady = false;

//    "ORDERED", "OFFLINE_PENDING", "ONLINE_PENDING", -> 오프라인 결제 - 결제 수단 추가
//    "OFFLINE_PAID", "ONLINE_PAID","BEFORE_USE", -> 체크인으로
//    "CHECK_IN" -> 체크아웃

  $scope.urlString = function (url) {
    console.log($scope.testPaymentData)
    paymentService.extendUserinfo($scope.testPaymentData);

    $location.url(url);

  };

  $scope.payreq = function () {

    console.log($scope.paymentinfo);
    console.log(112321)
  };
  $scope.seachDate =  new Date();
  $scope.lebelList =
      [ "MEMBER", "STAFF", "EMPLOYEE", "ADMIN", "DENIED"];
  $scope.filterState = "";
  $scope.orderState = {
    orderBy: "",
    reverse : false
  };
  $scope.checkMember = [];
  $scope.getCards = function (){
     gapi.client.nixgonspace.spaces.list(
         {maxResults:10000}
     ).execute(function(response) {
      $scope.isSpacesReady=true;
      angular.forEach(response.items, function(space){
        if(space.rfids !== null) {
          angular.forEach(space.rfids, function(rfids){
            $scope.cardList[rfids.toString()] = space;
            $scope.$apply();
          });
        }
      });
           console.log($scope.cardList)
     });
  };

  $scope.rfidsTest = function () {
//    console.log(1111);
    var tempStringArray = [];
    var resultArray = [];
    $http.get('rfids.csv').then(function(response){
      tempStringArray = response.data.split('\n');
      angular.forEach(tempStringArray,function (result) {
        result = result.split(',');
        if (result[2] !=="") {
          resultArray[result[0]] = [ result[1], result[2] ];
        } else {
          resultArray[result[0]] = [ result[1] ];
        }
      });

console.log(resultArray)
      gapi.client.nixgonspace.spaces.list().then(function (response) {

        angular.forEach(response.result.items, function(space) {
          console.log(space)

          var typeNumber = space.type + space.number;
          console.log(typeNumber , resultArray[typeNumber])
          if (resultArray[typeNumber] !==undefined) {
            space.rfids = resultArray[typeNumber];
            gapi.client.nixgonspace.spaces.update({
              spaceId:space.id,
              resource:space
            }).then(function (response) {
              console.log(typeNumber,response);
            });
          }
        });
      });
    });
  };
  $scope.rfidCheckINButton = function () {

    if ( $scope.cardList[$scope.rfidCheck] !== undefined && $scope.rfidCheck !=='' ) {
      $scope.rfidChk = true;
      gapi.client.nixgonspace.periods.list({
        timeMin: moment().startOf('day').format('YYYY-MM-DD'),
        timeMax: moment().startOf('day').format('YYYY-MM-DD')

      }).then(function (response) {
//        console.log(response)
        angular.forEach(response.result.items, function(period){
//          console.log(period)
          if($scope.cardList[$scope.rfidCheck].type === period.space.type &&
              $scope.cardList[$scope.rfidCheck].number === period.space.number) {
//            if(period.status ==='BOOKED'){
              $scope.checkMember.push(period);
//            }
            $scope.checkMember.space=period.space;
          }
        });
        if ($scope.checkMember.length>0) {
          $scope.errMsg ="";
          $scope.rfidChk = true;
        } else {
          $scope.rfidChk = false;
          $scope.checkMember=[];
          $scope.errMsg="해당되는 좌석에 예약된 내역이 존재하지 않습니다.";
        }
        $scope.$apply();
      });
    } else {
      // 카드 정보가 잘못되었습니다.
      $scope.errMsg = "카드 정보가 잘못되었습니다.";
      $scope.rfidChk = false;
      $scope.checkMember=[];
    }
  };
  $scope.$on('login', function() {
    var hrefString = window.location.href;
    var hrefArray = hrefString.split("/");
    var hrefThis = hrefArray[hrefArray.length-1];
    if (hrefThis === 'order' || hrefThis === 'inuse') {
      $scope.getOrders();
    } else  if (hrefThis === 'members') {
      $scope.getMembers();
    } else  if (hrefThis === 'checkin' || hrefThis === 'checkout' ) {
      $scope.getCards();
    }
  });
  $scope.orderbyChange = function (orderBy) {
    $scope.orderState.orderBy = orderBy;
    $scope.orderState.reverse = !$scope.orderState.reverse;
  };

  $scope.orderListFilter = function () {
    if ( ($scope.howToSearch !== undefined &&
          ($scope.searchInput !=='' && $scope.searchInput !==undefined )) ||
          ($scope.searchStatus !== '' && $scope.searchStatus !==undefined )) {
      $scope.orderList = [];
      var tempOrderList = [];
      angular.forEach($scope.originOrderList,function (travel) {
        if(travel.status === $scope.searchStatus || $scope.searchStatus ==="" || $scope.searchStatus === undefined){
          tempOrderList.push(travel);
        }
      });
      angular.forEach(tempOrderList, function (travel) {
        if($scope.howToSearch === undefined) {
          $scope.orderList.push(travel);
        } else if ($scope.howToSearch === 'fullName' || $scope.howToSearch === 'typeNumber' ||  $scope.howToSearch === 'orderId') {
          if (travel[$scope.howToSearch].match($scope.searchInput) !== null) {
            $scope.orderList.push(travel);
          }
        } else {
          if (travel.astronaut[$scope.howToSearch].match($scope.searchInput) !== null) {
            $scope.orderList.push(travel);
          }
        }
      });
    }
  };
  $scope.getMembers = function() {
    gapi.client.nixgonspace.astronauts.list().execute(function(response) {
      $scope.isSpacesReady=true;
      $scope.$apply(function(){
        $scope.userList = response.items;
      });
    });
  };

  $scope.getOrders = function() {

    gapi.client.nixgonspace.travels.list().execute(function(response) {
      console.log(response)
      $scope.$apply(function() {
        $scope.isSpacesReady = true;
        $scope.originOrderList = response.result.items;
        angular.forEach($scope.originOrderList,function(travel) {
          travel.fullName = travel.astronaut.familyName + travel.astronaut.givenName;
//          travel.typeNumber = travel.period.space.type+travel.period.space.number;
//          console.log(travel.period.start.getHours());

//          travel.period.start = moment(travel.period.start).format();
        });
        $scope.orderList = angular.copy($scope.originOrderList);
      });
    });
  };

  $scope.verifyChange = function(member) {
    console.log(member);
    if(member.verified === false){
      var titleString = member.familyName+member.givenName+"회원의 신분증을 확인 하였습니까?";
      var confirm = $mdDialog.confirm()
          .parent(angular.element(document.body))
          .content(titleString).ok('확인 완료').cancel('취소');
      $mdDialog.show(confirm).then(function() {
        member.verified = true;
        gapi.client.nixgonspace.astronauts.update({
          astronautId: member.email,
          resource: member
        }).then(function (response) {
          angular.forEach($scope.orderList, function (order) {
           if(order.astronautId === member.email){
             order.astronaut.verified=true;
           }
          });

          $mdToast.show( $mdToast.simple().content( member.familyName+member.givenName + "님의 본인 인증이 완료되었습니다." ) );
        });
      });
    }
  };

  $scope.updateLevel = function(member) {

    var titleString = member.familyName+member.givenName+"회원의 멤버십을 "+ $filter('translate')(member.level) +"(으)로 변경하겠습니다.";
    var confirm = $mdDialog.confirm()
        .parent(angular.element(document.body))
        .content(titleString).ok('확인 완료').cancel('취소');
    $mdDialog.show(confirm).then(function() {

      gapi.client.nixgonspace.astronauts.update({
        astronautId: member.email,
        resource: member
      }).then(function (response) {
        $mdToast.show( $mdToast.simple().content( "등급이 변경되었습니다." ) );
      });
    });
  };

  $scope.checkInOUT = function (period,status) {



    var titleString = period.familyName+period.givenName+"님의 상태를 "+ $filter('translate')(status) +"(으)로 변경하겠습니다.";
    var confirm = $mdDialog.confirm()
        .parent(angular.element(document.body))
        .content(titleString).ok('확인 완료').cancel('취소');
    $mdDialog.show(confirm).then(function() {
      period.status = status;
      console.log(period)
      gapi.client.nixgonspace.periods.update({
        periodId: period.id,
        resource: period
      }).then(function(response) {
        $mdToast.show( $mdToast.simple().content( $filter('translate')(status) +" 되었습니다." ) );
      });
    });



  };

  $scope.stateChange = function (travel) {
    $mdDialog.show({
      controller: DialogController,
//      templateUrl: 'admin/productDetail.html',
      templateUrl: 'admin/userInfoChange.html',
//      targetEvent: event,
      locals: {
        travel: travel,
        statusList:$scope.statusList
      }
    }).then(function(answer) {
      console.log(answer);
      if(answer !== undefined) {
        var index = $scope.originOrderList.indexOf(answer.travel);
        answer.travel.status=answer.status;
        $scope.originOrderList[index] = answer.travel;
        if ($scope.orderList.indexOf(answer.travel) !== -1){
          answer.travel.status=answer.status;
          $scope.orderList[$scope.orderList.indexOf(answer.travel)] = answer.travel;
        }
        console.log(answer.travel);
        gapi.client.nixgonspace.travels.update({
          travelId: answer.travel.id,
          resource: answer.travel
        }).then(function(response) {
          var status = "";
          if (answer.status === 'OFFLINE_PAID') {
            status = 'BOOKED';
          } else if (answer.status === 'ADMIN_CANCELED') {
            status = 'ADMIN_CANCELED';
          } else if (answer.status === 'ORDERED') {
            status = 'PREEMPTION';
          }

          angular.forEach(answer.travel.periods, function(period) {
            gapi.client.nixgonspace.periods.update({
              periodId : period,
              status : status
            }).then(function(response) {
              console.log(response);
            });
          });


          if (answer.noReturn) {
            var user = angular.copy(answer.travel.user);
            user.level = 'DENIED';
            gapi.client.nixgonspace.astronauts.update({
              astronaut_id: user.email,
              resource: user
            }).then(function (response) {
              console.log(response);
            });
          }

          $mdToast.show( $mdToast.simple().content('변경이 완료되었습니다.') );
        });
      }
    });
  };
  $scope.offlineCheckOut = function (travel, status) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'admin/OfflineCheckOut.html',
      targetEvent: event,
      locals: {
        travel: travel,
        statusList:$scope.statusList
      }
    }).then(function(answer) {

      console.log(answer)
      answer.status = status;
      gapi.client.nixgonspace.travels.update({
        travelId: answer.id,
        resource: answer,
      }).then(function(response) {
        console.log(response);
      });
//      if(answer !== undefined) {
//        var index = $scope.originOrderList.indexOf(answer.order);
//        answer.order.status=answer.status;
//        $scope.originOrderList[index] = answer.order;
//        if ($scope.orderList.indexOf(answer.order) !== -1){
//          answer.order.status=answer.status;
//          $scope.orderList[$scope.orderList.indexOf(answer.order)] = answer.order;
//        }
//          $mdToast.show(
//              $mdToast.simple().content(
//                  '변경이 완료되었습니다.'
//                  // $filter('translate')(' is canceled.')
//              )
//          );
//        });
//      }
    });


  };
function DialogController($scope, $mdDialog, travel, statusList) {

    $scope.category = 'Space';
    $scope.travel = travel;
    $scope.originTravel = travel;
    $scope.statusList=statusList;
    $scope.vacuums = [];
    $scope.status=travel.status;
    $scope.originStatus=angular.copy(travel.status);
    $scope.cardErrMsg = "";
    $scope.howToSearch="";
    $scope.resources=
    $scope.searchStatus="";
    $scope.searchDate ="";
    $scope.whenToDate ="";
    $scope.howToSearch ="";
    $scope.searchInput="";
    $scope.checkInput = false;
    $scope.cardNum="";
    $scope.spaceType="";
    $scope.number="";
    $scope.cardNumList={};
    $scope.noReturn=false;
    $scope.statusChangeCheck=false;
    $scope.statusChange = function (cardNum) {

      if ( $scope.status === 'OFFLINE_PAID'){
        $scope.statusChangeCheck = true;
      } else {
        $scope.statusChangeCheck = false;
      }

//      if($scope.originStatus !== $scope.status ){
//        if ( ($scope.status === 'CHECK_IN'  || $scope.status === 'CHECK_OUT') &&
//            $scope.order.events[0].space.category==='WORK' ) {
//          if ( $scope.cardNumCheck(cardNum) || $scope.noReturn === true) {
//            $scope.statusChangeCheck = true;
//          } else {
//            $scope.statusChangeCheck = false;
//          }
//        } else {
//          $scope.statusChangeCheck = true;
//        }
//      } else {
//        $scope.statusChangeCheck = false;
//      }

    };
    $scope.cardNumCheck = function (cardNum) {

      var primary_rfid = $scope.travel.events[0].space.primary_rfid;
      var secondary_rfid = $scope.travel.events[0].space.secondary_rfid;
      var returnCheck = false;

      if ( primary_rfid === cardNum || secondary_rfid === cardNum ) {
        $scope.cardErrMsg = "스페이스 카드의 정보가 일치합니다.";
        $scope.cardCheck = true;
      } else {
        $scope.cardErrMsg = "스페이스 카드 번호가 잘못 입력되었습니다.";
        $scope.cardCheck = false;
      }
      return $scope.cardCheck;
    };
//complete(order , order.status)
    $scope.hide = function() {

      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.payment = function () {
      console.log(order)
    };
    $scope.placeOrder = function (travel) {
      var result = {
        travel : travel,
        status : $scope.status
      };
      $mdDialog.hide(result);
    };
    $scope.answer = function(travel, status, noReturn) {

      var result = {
        travel : travel,
        status : $scope.status,
        noReturn: noReturn
      };
      $mdDialog.hide(result);
    };
  }


});
function doPay_ActiveX(){
          ret = xpay_check(document.getElementById('LGD_PAYINFO'), 'test');

          if (ret=="00"){     //ActiveX 로딩 성공
              var LGD_RESPCODE        = $scope.paymentinfo.LGD_RESPCODE;       	  //결과코드
              var LGD_RESPMSG         = $scope.paymentinfo.LGD_RESPMSG;        	  //결과메세지

              if( "0000" == LGD_RESPCODE ) { //결제성공
                var LGD_TID             = $scope.paymentinfo.LGD_TID;            //LG유플러스 거래번호
                var LGD_OID             = $scope.paymentinfo.LGD_OID;            //주문번호
                var LGD_PAYTYPE         = $scope.paymentinfo.LGD_PAYTYPE;        //결제수단
                var LGD_PAYDATE         = $scope.paymentinfo.LGD_PAYDATE;        //결제일자
                var LGD_FINANCECODE     = $scope.paymentinfo.LGD_FINANCECODE;    //결제기관코드
                var LGD_FINANCENAME     = $scope.paymentinfo.LGD_FINANCENAME;    //결제기관이름
                var LGD_FINANCEAUTHNUM  = $scope.paymentinfo.LGD_FINANCEAUTHNUM; //결제사승인번호
                var LGD_ACCOUNTNUM      = $scope.paymentinfo.LGD_ACCOUNTNUM;     //입금할 계좌 (가상계좌)
                var LGD_BUYER           = $scope.paymentinfo.LGD_BUYER;          //구매자명
                var LGD_PRODUCTINFO     = $scope.paymentinfo.LGD_PRODUCTINFO;    //상품명
                var LGD_AMOUNT          = $scope.paymentinfo.LGD_AMOUNT;         //결제금액
                var LGD_NOTEURL_RESULT  = $scope.paymentinfo.LGD_NOTEURL_RESULT; //상점DB처리(LGD_NOTEURL)결과 ('OK':정상,그외:실패)

                //메뉴얼의 결제결과 파라미터내용을 참고하시어 필요하신 파라미터를 추가하여 사용하시기 바랍니다.

                  var msg = "결제결과 : " + LGD_RESPMSG + "\n";
                  msg += "LG유플러스거래TID : " + LGD_TID +"\n";

                  if( LGD_NOTEURL_RESULT != "null" ) msg += LGD_NOTEURL_RESULT +"\n";
                  alert(msg);

                  document.getElementById('LGD_RESPCODE').value = LGD_RESPCODE;
                  document.getElementById('LGD_RESPMSG').value = LGD_RESPMSG;
                  document.getElementById('LGD_TID').value = LGD_TID;
                  document.getElementById('LGD_OID').value = LGD_OID;
                  document.getElementById('LGD_PAYTYPE').value = LGD_PAYTYPE;
                  document.getElementById('LGD_PAYDATE').value = LGD_PAYDATE;
                  document.getElementById('LGD_FINANCECODE').value = LGD_FINANCECODE;
                  document.getElementById('LGD_FINANCENAME').value = LGD_FINANCENAME;
                  document.getElementById('LGD_FINANCEAUTHNUM').value = LGD_FINANCEAUTHNUM;
                  document.getElementById('LGD_ACCOUNTNUM').value = LGD_ACCOUNTNUM;
                  document.getElementById('LGD_BUYER').value = LGD_BUYER;
                  document.getElementById('LGD_PRODUCTINFO').value = LGD_PRODUCTINFO;
                  document.getElementById('LGD_AMOUNT').value = LGD_AMOUNT;

                  document.getElementById('LGD_PAYINFO').submit();

              } else { //결제실패
                  alert("결제가 실패하였습니다. " + LGD_RESPMSG);
              }
          } else {
                  alert("LG유플러스 전자결제를 위한 ActiveX 설치 실패");
          }
      }
