package nixgonspace

import (
	"errors"
	"log"
	"time"

	"appengine/datastore"
	"appengine/mail"
	"appengine/user"

	"github.com/GoogleCloudPlatform/go-endpoints/endpoints"
	twilio "github.com/streamrail/twilio-gae"
)

const clientId = ""

var (
	scopes    = []string{endpoints.EmailScope}
	clientIds = []string{clientId, endpoints.APIExplorerClientID}
	audiences = []string{clientId}
)

func getEndpointsCurrentUser(c endpoints.Context) (*user.User, error) {
	u, err := endpoints.CurrentUser(c, scopes, audiences, clientIds)
	if err != nil {
		return nil, err
	}
	if u == nil {
		return nil, errors.New("Unauthorized: Login Required.")
	}

	return u, nil
}

type QueryMarker struct {
	datastore.Cursor
}

func (qm *QueryMarker) MarshalJSON() ([]byte, error) {
	return []byte(`"` + qm.String() + `"`), nil
}

func (qm *QueryMarker) UnmarshalJSON(buf []byte) error {
	if len(buf) < 2 || buf[0] != '"' || buf[len(buf)-1] != '"' {
		return errors.New("QueryMarker: bad cursor value")
	}
	cursor, err := datastore.DecodeCursor(string(buf[1 : len(buf)-1]))
	if err != nil {
		return err
	}
	*qm = QueryMarker{cursor}
	return nil
}

const (
	OpeningTime   = 9
	ClosingTime   = 22
	BusinessHours = ClosingTime - OpeningTime
)

type SpaceAPI struct{}

const (
	DefaultPoint = 0

	Denied   = "DENIED"
	Member   = "MEMBER"
	Staff    = "STAFF"
	Employee = "EMPLOYEE"
	Admin    = "ADMIN"
)

type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (*SpaceAPI) Login(c endpoints.Context, r *LoginReq) (*LoginReq, error) {
	key := datastore.NewKey(c, "Astronaut", r.Email, 0, nil)
	astronaut := &Astronaut{}
	if err := datastore.Get(c, key, astronaut); err != nil {
		return nil, err
	}
	if r.Password != astronaut.Password {
		return nil, endpoints.NewInternalServerError("password mismatch")
	}
	//err := bcrypt.CompareHashAndPassword(astronaut.Password, r.Password)
	//if err != nil {
	//	return nil, err
	//}
	return r, nil
}

type Astronaut struct {
	Email        string `json:"email"`
	FamilyName   string `json:"familyName"`
	GivenName    string `json:"givenName"`
	MobileNumber string `json:"mobileNumber"`
	Verified     bool   `json:"verified" endpoints:"d=false"`
	Created      string `json:"created"`
	Point        int    `json:"point"`
	Level        string `json:"level"`
	Notes        string `json:"notes"`
	Picture      string `json:"picture"`
	Password     string `json:"password"`
}

func (*SpaceAPI) InsertAstronaut(c endpoints.Context, r *Astronaut) (*Astronaut, error) {
	r.Created = time.Now().Format(time.RFC3339Nano)
	r.Point = DefaultPoint
	//password, err := bcrypt.GenerateFromPassword([]byte(r.Password), 10)
	//r.Password = string(password)

	key := datastore.NewKey(c, "Astronaut", r.Email, 0, nil)
	key, err := datastore.Put(c, key, r)
	if err != nil {
		return nil, err
	}

	//r.Key = key.StringID()
	return r, nil
}

type AstronautIdReq struct {
	AstronautId string `json:"astronautId"`
}

func (*SpaceAPI) GetAstronaut(c endpoints.Context, r *AstronautIdReq) (*Astronaut, error) {
	key := datastore.NewKey(c, "Astronaut", r.AstronautId, 0, nil)

	astronaut := &Astronaut{}
	if err := datastore.Get(c, key, astronaut); err != nil {
		return nil, err
	}
	//astronaut.Key = key.StringID()
	return astronaut, nil
}

type AstronautReq struct {
	AstronautIdReq
	Astronaut
}

func (*SpaceAPI) UpdateAstronaut(c endpoints.Context, r *AstronautReq) (*Astronaut, error) {
	key := datastore.NewKey(c, "Astronaut", r.AstronautId, 0, nil)

	astronaut := &Astronaut{}
	if err := datastore.Get(c, key, astronaut); err != nil {
		return nil, err
	}

	if r.FamilyName != "" {
		astronaut.FamilyName = r.FamilyName
	}
	if r.GivenName != "" {
		astronaut.GivenName = r.GivenName
	}
	if r.MobileNumber != "" {
		astronaut.MobileNumber = r.MobileNumber
	}
	if r.Verified {
		astronaut.Verified = r.Verified
	}
	if r.Point < 0 {
		astronaut.Point = r.Point
	}
	if r.Level != "" {
		astronaut.Level = r.Level
	}
	if r.Notes != "" {
		astronaut.Notes = r.Notes
	}
	if r.Picture != "" {
		astronaut.Picture = r.Picture
	}

	if _, err := datastore.Put(c, key, astronaut); err != nil {
		return nil, err
	}
	//astronaut.Key = key.StringID()
	return astronaut, nil
}

func (*SpaceAPI) DeleteAstronaut(c endpoints.Context, r *AstronautIdReq) (*endpoints.VoidMessage, error) {
	key := datastore.NewKey(c, "Astronaut", r.AstronautId, 0, nil)
	if err := datastore.Delete(c, key); err != nil {
		return nil, err
	}
	return &endpoints.VoidMessage{}, nil
}

type AstronautsReq struct {
	MaxResults int          `json:"maxResults" endpoints:"d=100"`
	PageToken  *QueryMarker `json:"pageToken"`
}

type Astronauts struct {
	NextPageToken *QueryMarker `json:"nextPageToken,omitempty"`
	Items         []*Astronaut `json:"items"`
}

func (*SpaceAPI) ListAstronauts(c endpoints.Context, r *AstronautsReq) (*Astronauts, error) {
	if r.MaxResults <= 0 {
		r.MaxResults = 100
	}
	items := make([]*Astronaut, 0, r.MaxResults)

	q := datastore.NewQuery("Astronaut").Limit(r.MaxResults)
	if r.PageToken != nil {
		q = q.Start(r.PageToken.Cursor)
	}

	var iter *datastore.Iterator
	for iter = q.Run(c); ; {
		var astronaut Astronaut
		_, err := iter.Next(&astronaut)
		if err == datastore.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		//astronaut.Key = key.StringID()
		items = append(items, &astronaut)
	}

	cur, err := iter.Cursor()
	if err != nil {
		return nil, err
	}
	return &Astronauts{&QueryMarker{cur}, items}, nil
}

const (
	// Countries of a space
	SouthKorea = "KR"

	// Cities of a space
	Busan = "BUSAN"

	// Camps of a space
	Seomyeon = "SEOMYEON"

	// Categories of a space
	Event = "EVENT"
	Work  = "WORK"

	// Types of a space
	Coworking     = "CS"
	CoworkingDual = "CSD"
	Floworking    = "FS"
	Bright        = "BS"
	Insight       = "IS"
	InsightSix    = "ISS"
)

type Space struct {
	Key      *datastore.Key `json:"id" datastore:"-"`
	Country  string         `json:"country"`
	City     string         `json:"city"`
	Camp     string         `json:"camp"`
	Category string         `json:"category"`
	Type     string         `json:"type"`
	Number   int            `json:"number"`
	RFIDs    []string       `json:"rfids"`
}

func (*SpaceAPI) InsertSpace(c endpoints.Context, r *Space) (*Space, error) {
	key := datastore.NewIncompleteKey(c, "Space", nil)
	key, err := datastore.Put(c, key, r)
	if err != nil {
		return nil, err
	}
	r.Key = key
	return r, nil
}

type SpaceIdReq struct {
	SpaceId *datastore.Key `json:"spaceId"`
}

func (*SpaceAPI) GetSpace(c endpoints.Context, r *SpaceIdReq) (*Space, error) {
	space := &Space{}
	if err := datastore.Get(c, r.SpaceId, space); err != nil {
		return nil, err
	}
	space.Key = r.SpaceId
	return space, nil
}

type SpaceReq struct {
	SpaceIdReq
	Space
}

func (*SpaceAPI) UpdateSpace(c endpoints.Context, r *SpaceReq) (*Space, error) {
	space := &Space{}
	if err := datastore.Get(c, r.SpaceId, space); err != nil {
		return nil, err
	}

	// TODO: Update space entity
	if r.Country != "" {
		space.Country = r.Country
	}
	if r.City != "" {
		space.City = r.City
	}
	if r.Camp != "" {
		space.Camp = r.Camp
	}
	if r.Category != "" {
		space.Category = r.Category
	}
	if r.Type != "" {
		space.Type = r.Type
	}
	if r.Number > 0 {
		space.Number = r.Number
	}
	// TODO: Update RFIDs
	if len(r.RFIDs) > 0 {
		for _, id := range r.RFIDs {
			space.RFIDs = append(space.RFIDs, id)
		}
	}

	if _, err := datastore.Put(c, r.SpaceId, space); err != nil {
		return nil, err
	}
	space.Key = r.SpaceId
	return space, nil
}

func (*SpaceAPI) DeleteSpace(c endpoints.Context, r *SpaceIdReq) (*endpoints.VoidMessage, error) {
	if err := datastore.Delete(c, r.SpaceId); err != nil {
		return nil, err
	}
	return &endpoints.VoidMessage{}, nil
}

type SpacesReq struct {
	MaxResults int          `json:"maxResults" endpoints:"d=200"`
	PageToken  *QueryMarker `json:"pageToken"`
	Type       string       `json:"type"`
}

type Spaces struct {
	NextPageToken *QueryMarker `json:"nextPageToken,omitempty"`
	Items         []*Space     `json:"items"`
}

func (*SpaceAPI) ListSpaces(c endpoints.Context, r *SpacesReq) (*Spaces, error) {
	if r.MaxResults <= 0 {
		r.MaxResults = 200
	}
	items := make([]*Space, 0, r.MaxResults)

	q := datastore.NewQuery("Space").Limit(r.MaxResults)
	if r.Type != "" {
		q = q.Filter("Type =", r.Type)
	}

	if r.PageToken != nil {
		q = q.Start(r.PageToken.Cursor)
	}

	var iter *datastore.Iterator
	for iter = q.Run(c); ; {
		var space Space
		key, err := iter.Next(&space)
		if err == datastore.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		space.Key = key
		items = append(items, &space)
	}

	cur, err := iter.Cursor()
	if err != nil {
		return nil, err
	}
	return &Spaces{&QueryMarker{cur}, items}, nil
}

const (
	// Categories of a rocket
	RocketCategorySpace = "SPACE"

	// Types of a rocket
	RocketTypeMonthly = "MONTHLY"
	RocketTypeDaily   = "DAILY"
	RocketTypeEvening = "EVENING"
	RocketTypeHourly  = "HOURLY"
)

type Rocket struct {
	Key         *datastore.Key `json:"id" datastore:"-"`
	Code        string         `json:"code"`
	Title       string         `json:"title"`
	Description string         `json:"description"`
	Category    string         `json:"category"`
	Type        string         `json:"type"`
	SpaceType   string         `json:"spaceType"`
	Price       int            `json:"price"`
}

func (*SpaceAPI) InsertRocket(c endpoints.Context, r *Rocket) (*Rocket, error) {
	key := datastore.NewIncompleteKey(c, "Rocket", nil)
	key, err := datastore.Put(c, key, r)
	if err != nil {
		return nil, err
	}
	r.Key = key
	return r, nil
}

type RocketIdReq struct {
	RocketId *datastore.Key `json:"rocketId"`
}

func (*SpaceAPI) GetRocket(c endpoints.Context, r *RocketIdReq) (*Rocket, error) {
	rocket := &Rocket{}
	if err := datastore.Get(c, r.RocketId, rocket); err != nil {
		return nil, err
	}
	rocket.Key = r.RocketId
	return rocket, nil
}

type RocketReq struct {
	RocketIdReq
	Rocket
}

func (*SpaceAPI) UpdateRocket(c endpoints.Context, r *RocketReq) (*Rocket, error) {
	rocket := &Rocket{}
	if err := datastore.Get(c, r.RocketId, rocket); err != nil {
		return nil, err
	}

	if r.Title != "" {
		rocket.Title = r.Title
	}
	if r.Description != "" {
		rocket.Description = r.Description
	}
	if r.Category != "" {
		rocket.Category = r.Category
	}
	if r.Type != "" {
		rocket.Type = r.Type
	}
	if r.SpaceType != "" {
		rocket.SpaceType = r.SpaceType
	}
	if r.Price < 0 {
		rocket.Price = r.Price
	}

	if _, err := datastore.Put(c, r.RocketId, rocket); err != nil {
		return nil, err
	}
	rocket.Key = r.RocketId
	return rocket, nil
}

func (*SpaceAPI) DeleteRocket(c endpoints.Context, r *RocketIdReq) (*endpoints.VoidMessage, error) {
	if err := datastore.Delete(c, r.RocketId); err != nil {
		return nil, err
	}
	return &endpoints.VoidMessage{}, nil
}

type RocketsReq struct {
	MaxResults int          `json:"maxResults" endpoints:"d=100"`
	PageToken  *QueryMarker `json:"pageToken"`
}

type Rockets struct {
	NextPageToken *QueryMarker `json:"nextPageToken,omitempty"`
	Items         []*Rocket    `json:"items"`
}

func (*SpaceAPI) ListRockets(c endpoints.Context, r *RocketsReq) (*Rockets, error) {
	if r.MaxResults <= 0 {
		r.MaxResults = 100
	}
	items := make([]*Rocket, 0, r.MaxResults)

	q := datastore.NewQuery("Rocket").Limit(r.MaxResults)
	if r.PageToken != nil {
		q = q.Start(r.PageToken.Cursor)
	}

	var iter *datastore.Iterator
	for iter = q.Run(c); ; {
		var rocket Rocket
		key, err := iter.Next(&rocket)
		if err == datastore.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		rocket.Key = key
		items = append(items, &rocket)
	}

	cur, err := iter.Cursor()
	if err != nil {
		return nil, err
	}
	return &Rockets{&QueryMarker{cur}, items}, nil
}

const (
	PeriodStatusFree           = "FREE"
	PeriodStatusPreemption     = "PREEMPTION"
	PeriodStatusBooked         = "BOOKED"
	PeriodStatusCheckIn        = "CHECK_IN"
	PeriodStatusCheckOut       = "CHECK_OUT"
	PeriodStatusUserCanceled   = "USER_CANCELED"
	PeriodStatusAdminCanceled  = "ADMIN_CANCELED"
	PeriodStatusSystemCanceled = "SYSTEM_CANCELED"
)

type Period struct {
	Key         *datastore.Key `json:"id" datastore:"-"`
	AstronautID string         `json:"astronautId"`
	Space       Space          `json:"space"`
	Start       time.Time      `json:"start"`
	End         time.Time      `json:"end"`
	Duration    int            `json:"duration"`
	Status      string         `json:"status"`
	CheckIn     time.Time      `json:"checkIn"`
	CheckOut    time.Time      `json:"checkOut"`
	Created     time.Time      `json:"created"`
	Updated     time.Time      `json:"updated"`
}

type PeriodResp struct {
	Period
	Astronaut
}

func (*SpaceAPI) InsertPeriod(c endpoints.Context, r *Period) (*PeriodResp, error) {
	r.Created = time.Now()
	r.Updated = time.Now()

	key := datastore.NewIncompleteKey(c, "Period", nil)
	key, err := datastore.Put(c, key, r)
	if err != nil {
		return nil, err
	}
	r.Key = key

	var astronaut Astronaut
	key = datastore.NewKey(c, "Astronaut", r.AstronautID, 0, nil)
	if err = datastore.Get(c, key, &astronaut); err != nil {
		return nil, err
	}

	return &PeriodResp{*r, astronaut}, nil
}

type PeriodIdReq struct {
	PeriodKey *datastore.Key `json:"periodId"`
}

func (*SpaceAPI) GetPeriod(c endpoints.Context, r *PeriodIdReq) (*PeriodResp, error) {
	var period Period
	if err := datastore.Get(c, r.PeriodKey, &period); err != nil {
		return nil, err
	}
	period.Key = r.PeriodKey

	var astronaut Astronaut
	key := datastore.NewKey(c, "Astronaut", period.AstronautID, 0, nil)
	if err := datastore.Get(c, key, &astronaut); err != nil {
		return nil, err
	}

	return &PeriodResp{period, astronaut}, nil
}

type PeriodReq struct {
	PeriodIdReq
	Period
}

func (*SpaceAPI) UpdatePeriod(c endpoints.Context, r *PeriodReq) (*PeriodResp, error) {
	period := &Period{}
	if err := datastore.Get(c, r.PeriodKey, period); err != nil {
		return nil, err
	}

	if &r.Space != nil {
		period.Space = r.Space
	}
	if &r.Start != nil {
		period.Start = r.Start
	}
	if r.Duration != period.Duration {
		period.Duration = r.Duration
	}
	if r.Status != "" {
		period.Status = r.Status
	}
	if &r.CheckIn != nil {
		period.CheckIn = r.CheckIn
	}
	if &r.CheckOut != nil {
		period.CheckOut = r.CheckOut
	}
	period.Updated = time.Now()

	if _, err := datastore.Put(c, r.PeriodKey, period); err != nil {
		return nil, err
	}
	period.Key = r.PeriodKey

	var astronaut Astronaut
	key := datastore.NewKey(c, "Astronaut", period.AstronautID, 0, nil)
	if err := datastore.Get(c, key, &astronaut); err != nil {
		return nil, err
	}

	return &PeriodResp{*period, astronaut}, nil
}

func (*SpaceAPI) DeletePeriod(c endpoints.Context, r *PeriodIdReq) (*endpoints.VoidMessage, error) {
	if err := datastore.Delete(c, r.PeriodKey); err != nil {
		return nil, err
	}
	return &endpoints.VoidMessage{}, nil
}

type PeriodsReq struct {
	MaxResults  int            `json:"maxResults" endpoints:"d=200"`
	PageToken   *QueryMarker   `json:"pageToken"`
	TimeMin     string         `json:"timeMin"`
	TimeMax     string         `json:"timeMax"`
	SpaceType   string         `json:"spaceType"`
	SpaceKey    *datastore.Key `json:"spaceId"`
	SpaceNumber int            `json:"spaceNumber"`
	AstronautID string         `json:"astronautId"`
}

type Periods struct {
	NextPageToken *QueryMarker  `json:"nextPageToken,omitempty"`
	Items         []*PeriodResp `json:"items"`
}

func (*SpaceAPI) ListPeriods(c endpoints.Context, r *PeriodsReq) (*Periods, error) {
	if r.MaxResults <= 0 {
		r.MaxResults = 200
	}
	items := make([]*PeriodResp, 0, r.MaxResults)

	q := datastore.NewQuery("Period").Limit(r.MaxResults)

	if timeMin, err := time.Parse(time.RFC3339Nano, r.TimeMin); err == nil {
		c.Debugf("%v", timeMin)
		q = q.Filter("Start >=", timeMin)
	}
	if timeMax, err := time.Parse(time.RFC3339Nano, r.TimeMax); err == nil {
		c.Debugf("%v", timeMax)
		q = q.Filter("Start <=", timeMax)
	}
	if r.SpaceType != "" {
		q = q.Filter("Space.Type =", r.SpaceType)
	}
	if r.SpaceKey != nil {
		c.Debugf("SpaceKey: %v", r.SpaceKey)
		q = q.Filter("Space.__key__ =", r.SpaceKey)
	}
	if r.SpaceNumber > 0 {
		q = q.Filter("Space.Number =", r.SpaceNumber)
	}
	if r.AstronautID != "" {
		q = q.Filter("AstronautID =", r.AstronautID)
	}
	if r.PageToken != nil {
		q = q.Start(r.PageToken.Cursor)
	}

	var iter *datastore.Iterator
	for iter = q.Run(c); ; {
		var period Period
		key, err := iter.Next(&period)
		if err == datastore.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		period.Key = key

		var astronaut Astronaut
		key = datastore.NewKey(c, "Astronaut", period.AstronautID, 0, nil)
		if err = datastore.Get(c, key, &astronaut); err != nil {
			return nil, err
		}
		items = append(items, &PeriodResp{period, astronaut})
	}

	cur, err := iter.Cursor()
	if err != nil {
		return nil, err
	}
	return &Periods{&QueryMarker{cur}, items}, nil
}

const (
	// Methods of a payment
	Undefined = "UNDEFINED"
	Cash      = "CASH"
	Card      = "CARD"
)

type Payment struct {
	IsOnline bool   `json:"isOnline"`
	Method   string `json:"method"`
}

const (
	// Statuses of a travel
	TravelStatusOrdered        = "ORDERED"
	TravelStatusOfflinePending = "OFFLINE_PENDING"
	TravelStatusOnlinePending  = "ONLINE_PENDING"
	TravelStatusOfflinePaid    = "OFFLINE_PAID"
	TravelStatusOnlinePaid     = "ONLINE_PAID"
	TravelStatusInUse          = "IN_USE"
	TravelStatusCompleteUse    = "COMPLETE_USE"
	TravelStatusUserCanceled   = "USER_CANCELED"
	TravelStatusAdminCanceled  = "ADMIN_CANCELED"
	TravelStatusSystemCanceled = "SYSTEM_CANCELED"
)

type EventInfo struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Attendance  int    `json:"attendance"`
	Organizer   string `json:"organizer"`
	Contacts    string `json:"contacts"`
}

type Travel struct {
	Key          *datastore.Key   `json:"id" datastore:"-"`
	OrderID      int64            `json:"orderId,string"`
	AstronautKey string           `json:"astronautId"`
	RocketKey    *datastore.Key   `json:"rocketId"`
	Quantity     int              `json:"quantity"`
	Status       string           `json:"status"`
	Created      time.Time        `json:"created"`
	Updated      time.Time        `json:"updated"`
	Payment      Payment          `json:"payment"`
	Price        int              `json:"price"`
	UsedPoint    int              `json:"usedPoint"`
	PeriodIDs    []*datastore.Key `json:"periodIds"`
}

func (api *SpaceAPI) InsertTravel(c endpoints.Context, r *Travel) (*TravelResp, error) {
	r.Created = time.Now()
	r.Updated = time.Now()

	key := datastore.NewIncompleteKey(c, "Travel", nil)
	key, err := datastore.Put(c, key, r)
	if err != nil {
		c.Debugf("datastore.Put error: %v", err)
		return nil, err
	}

	astronaut := &Astronaut{}
	astronautKey := datastore.NewKey(c, "Astronaut", r.AstronautKey, 0, nil)
	if err := datastore.Get(c, astronautKey, astronaut); err != nil {
		return nil, err
	}

	rocket := &Rocket{}
	if err := datastore.Get(c, r.RocketKey, rocket); err != nil {
		return nil, err
	}
	rocket.Key = r.RocketKey

	r.Key = key
	r.OrderID = key.IntID()

	return &TravelResp{*r, *astronaut, *rocket, nil}, err
}

type TravelIdReq struct {
	TravelKey *datastore.Key `json:"travelId"`
}

type TravelResp struct {
	Travel
	Astronaut Astronaut `json:"astronaut"`
	Rocket    Rocket    `json:"rocket"`
	Periods   []*Period `json:"periods"`
}

func (api *SpaceAPI) GetTravel(c endpoints.Context, r *TravelIdReq) (*TravelResp, error) {
	travel := &Travel{}
	if err := datastore.Get(c, r.TravelKey, travel); err != nil {
		return nil, err
	}

	astronaut := &Astronaut{}
	astronautKey := datastore.NewKey(c, "Astronaut", travel.AstronautKey, 0, nil)
	if err := datastore.Get(c, astronautKey, astronaut); err != nil {
		return nil, err
	}

	rocket := &Rocket{}
	if err := datastore.Get(c, travel.RocketKey, rocket); err != nil {
		return nil, err
	}
	rocket.Key = travel.RocketKey

	travel.Key = r.TravelKey
	travel.OrderID = r.TravelKey.IntID()

	periods := make([]*Period, len(travel.PeriodIDs))
	if err := datastore.GetMulti(c, travel.PeriodIDs, periods); err != nil {
		return nil, err
	}
	return &TravelResp{*travel, *astronaut, *rocket, periods}, nil
}

type TravelReq struct {
	TravelIdReq
	Travel
}

func (api *SpaceAPI) UpdateTravel(c endpoints.Context, r *TravelReq) (*TravelResp, error) {
	travel := &Travel{}
	if err := datastore.Get(c, r.TravelKey, travel); err != nil {
		return nil, err
	}

	travel.Status = r.Status
	travel.Updated = time.Now()

	if _, err := datastore.Put(c, r.TravelKey, travel); err != nil {
		return nil, err
	}

	astronaut := &Astronaut{}
	astronautKey := datastore.NewKey(c, "Astronaut", r.AstronautKey, 0, nil)
	if err := datastore.Get(c, astronautKey, astronaut); err != nil {
		return nil, err
	}

	rocket := &Rocket{}
	if err := datastore.Get(c, r.RocketKey, rocket); err != nil {
		return nil, err
	}
	rocket.Key = r.RocketKey

	travel.Key = r.TravelKey
	travel.OrderID = r.TravelKey.IntID()

	periods := make([]*Period, len(travel.PeriodIDs))
	if err := datastore.GetMulti(c, travel.PeriodIDs, periods); err != nil {
		return nil, err
	}
	return &TravelResp{*travel, *astronaut, *rocket, periods}, nil
}

func (*SpaceAPI) DeleteTravel(c endpoints.Context, r *TravelIdReq) (*endpoints.VoidMessage, error) {
	if err := datastore.Delete(c, r.TravelKey); err != nil {
		return nil, err
	}
	return &endpoints.VoidMessage{}, nil
}

type TravelsReq struct {
	MaxResults   int          `json:"maxResults" endpoints:"d=200"`
	PageToken    *QueryMarker `json:"pageToken"`
	TimeMin      string       `json:"timeMin"`
	TimeMax      string       `json:"timeMax"`
	AstronautKey string       `json:"astronautId"`
	SpaceType    string       `json:"spaceType"`
	SpaceNumber  int          `json:"spaceNumber"`
	Status       string       `json:"status"`
}

type Travels struct {
	NextPageToken *QueryMarker  `json:"nextPageToken,omitempty"`
	Items         []*TravelResp `json:"items"`
}

func (*SpaceAPI) ListTravels(c endpoints.Context, r *TravelsReq) (*Travels, error) {
	if r.MaxResults <= 0 {
		r.MaxResults = 200
	}
	items := make([]*TravelResp, 0, r.MaxResults)

	q := datastore.NewQuery("Travel").Limit(r.MaxResults)
	if timeMin, err := time.Parse(time.RFC3339Nano, r.TimeMin); err == nil {
		q = q.Filter("Date >=", timeMin)
	}
	if timeMax, err := time.Parse(time.RFC3339Nano, r.TimeMax); err == nil {
		q = q.Filter("Date <=", timeMax)
	}
	if r.AstronautKey != "" {
		q = q.Filter("AstronautKey =", r.AstronautKey)
	}
	if r.SpaceType != "" {
		q = q.Filter("Period.Space.Type =", r.SpaceType)
	}
	if r.SpaceNumber > 0 {
		q = q.Filter("Period.Space.Number =", r.SpaceNumber)
	}
	if r.Status != "" {
		q = q.Filter("Status =", r.Status)
	}

	if r.PageToken != nil {
		q = q.Start(r.PageToken.Cursor)
	}

	var iter *datastore.Iterator
	for iter = q.Run(c); ; {
		var travel Travel
		key, err := iter.Next(&travel)
		if err == datastore.Done {
			break
		}
		if err != nil {
			return nil, err
		}
		travel.Key = key
		travel.OrderID = key.IntID()

		astronaut := &Astronaut{}
		astronautKey := datastore.NewKey(c, "Astronaut", travel.AstronautKey, 0, nil)
		if err := datastore.Get(c, astronautKey, astronaut); err != nil {
			c.Debugf("astro: %v", err)
			return nil, err
		}

		rocket := &Rocket{}
		if err := datastore.Get(c, travel.RocketKey, rocket); err != nil {
			c.Debugf("rocket: %v", err)
			return nil, err
		}
		rocket.Key = travel.RocketKey

		periods := make([]*Period, len(travel.PeriodIDs))
		if err := datastore.GetMulti(c, travel.PeriodIDs, periods); err != nil {
			return nil, err
		}
		items = append(items, &TravelResp{travel, *astronaut, *rocket, periods})
	}

	cur, err := iter.Cursor()
	if err != nil {
		return nil, err
	}
	return &Travels{&QueryMarker{cur}, items}, nil
}

type Order struct {
	Travel `json:"travel"`
	Period `json:"period"`
}

func (api *SpaceAPI) InsertOrder(c endpoints.Context, r *Order) (*Order, error) {
	rocket := &Rocket{}
	if err := datastore.Get(c, r.RocketKey, rocket); err != nil {
		c.Debugf("%v", err)
		return nil, err
	}

	periodIDs := make([]*datastore.Key, 0, 1)

	now := time.Now()
	r.Travel.Created, r.Travel.Updated = now, now
	r.Period.Created, r.Period.Updated = now, now

	if rocket.Type != RocketTypeHourly {
		durationUnit := 1
		if rocket.Type == RocketTypeMonthly {
			durationUnit = 30
		}

		for i := 0; i < r.Quantity*durationUnit; i++ {
			r.Period.Start = r.Period.Start.AddDate(0, 0, i)

			key := datastore.NewIncompleteKey(c, "Period", nil)
			key, err := datastore.Put(c, key, &r.Period)
			if err != nil {
				return nil, err
			}
			periodIDs = append(periodIDs, key)
		}

	} else {
		key := datastore.NewIncompleteKey(c, "Period", nil)
		key, err := datastore.Put(c, key, &r.Period)
		if err != nil {
			c.Debugf("period: %v", r.Period)
			c.Debugf("period: %v", err)
			return nil, err
		}
		periodIDs = append(periodIDs, key)
	}

	r.Travel.PeriodIDs = periodIDs
	key := datastore.NewIncompleteKey(c, "Travel", nil)
	key, err := datastore.Put(c, key, &r.Travel)
	if err != nil {
		c.Debugf("travel: %v", err)
		return nil, err
	}
	r.Travel.Key = key
	r.OrderID = key.IntID()

	return r, nil
}

type space struct {
	Category string `json:"category"`
	Type     string `json:"type"`
	Amount   int    `json:"amount"`
}

type Bang struct {
	Country    string       `json:"country"`
	City       string       `json:"city"`
	Camp       string       `json:"camp"`
	Spaces     []*space     `json:"spaces"`
	Astronauts []*Astronaut `json:"astronauts"`
	Rockets    []*Rocket    `json:"rockets"`
	Travels    []*Travel    `json:"travels"`
}

func (api *SpaceAPI) InsertBang(c endpoints.Context, r *Bang) (*Bang, error) {
	for _, s := range r.Spaces {
		for i := 1; i <= s.Amount; i++ {
			space := &Space{
				nil,
				r.Country,
				r.City,
				r.Camp,
				s.Category,
				s.Type,
				i,
				make([]string, 0, 2),
			}
			_, err := api.InsertSpace(c, space)
			if err != nil {
				return nil, err
			}
		}
	}

	for _, a := range r.Astronauts {
		_, err := api.InsertAstronaut(c, a)
		if err != nil {
			return nil, err
		}
	}

	for _, r := range r.Rockets {
		_, err := api.InsertRocket(c, r)
		if err != nil {
			return nil, err
		}
	}

	for _, t := range r.Travels {
		_, err := api.InsertTravel(c, t)
		if err != nil {
			c.Debugf("%v", err)
			return nil, err
		}
	}
	return r, nil
}

func (api *SpaceAPI) UpdateBang(c endpoints.Context, r *Bang) (*Bang, error) {
	for _, t := range r.Travels {
		_, err := api.InsertTravel(c, t)
		if err != nil {
			return nil, err
		}
	}
	return r, nil
}

const (
	TwilioAccountSID = "AC28a1eee7c0a00d57ef3e34cda271978b"
	TwilioAuthToken  = "de02034d4e330005967aba61f6fcd696"
	TwilioFrom       = "+15704715555"
)

type SMSMsg struct {
	To   string `json:"to"`
	Body string `json:"body"`
}

func (*SpaceAPI) InsertSMSMsg(c endpoints.Context, r *SMSMsg) (*SMSMsg, error) {
	client := twilio.NewClient(TwilioAccountSID, TwilioAuthToken)

	_, err := twilio.NewMessage(c, client, TwilioFrom, r.To, twilio.Body(r.Body))
	if err != nil {
		return nil, err
	}
	return r, nil
}

type Mail struct {
	To      []string `json:"to"`
	Subject string   `json:"subject"`
	Body    string   `json:"body"`
}

func (*SpaceAPI) InsertMail(c endpoints.Context, r *Mail) (*Mail, error) {
	msg := &mail.Message{
		Sender:  "NIXGON SPACE <admin@nixgon.com>",
		To:      r.To,
		Subject: r.Subject,
		Body:    r.Body,
	}
	if err := mail.Send(c, msg); err != nil {
		return nil, err
	}
	return r, nil
}

func init() {
	api, err := endpoints.RegisterService(&SpaceAPI{}, "nixgonspace", "v1", "NIXGON SPACE API", true)
	if err != nil {
		log.Fatal(err)
	}

	register := func(handler, name, method, path, desc string) {
		m := api.MethodByName(handler)
		if m == nil {
			log.Fatalf("Method %s cannot found.", handler)
		}
		i := m.Info()
		i.Name, i.HTTPMethod, i.Path, i.Desc = name, method, path, desc
	}

	register("Login", "auth.login", "POST", "nixgonspace/auth", "")
	register("InsertAstronaut", "astronauts.insert", "POST", "nixgonspace/astronauts", "")
	register("GetAstronaut", "astronauts.get", "GET", "nixgonspace/astronauts/{astronautId}", "")
	register("UpdateAstronaut", "astronauts.update", "PUT", "nixgonspace/astronauts/{astronautId}", "")
	register("DeleteAstronaut", "astronauts.delete", "DELETE", "nixgonspace/astronauts/{astronautId}", "")
	register("ListAstronauts", "astronauts.list", "GET", "nixgonspace/astronauts", "")

	register("InsertSpace", "spaces.insert", "POST", "nixgonspace/spaces", "")
	register("GetSpace", "spaces.get", "GET", "nixgonspace/spaces/{spaceId}", "")
	register("UpdateSpace", "spaces.update", "PUT", "nixgonspace/spaces/{spaceId}", "")
	register("DeleteSpace", "spaces.delete", "DELETE", "nixgonspace/spaces/{spaceId}", "")
	register("ListSpaces", "spaces.list", "GET", "nixgonspace/spaces", "")

	register("InsertRocket", "rockets.insert", "POST", "nixgonspace/rockets", "")
	register("GetRocket", "rockets.get", "GET", "nixgonspace/rockets/{rocketId}", "")
	register("UpdateRocket", "rockets.update", "PUT", "nixgonspace/rockets/{rocketId}", "")
	register("DeleteRocket", "rockets.delete", "DELETE", "nixgonspace/rockets/{rocketId}", "")
	register("ListRockets", "rockets.list", "GET", "nixgonspace/rockets", "")

	register("InsertPeriod", "periods.insert", "POST", "nixgonspace/periods", "")
	register("GetPeriod", "periods.get", "GET", "nixgonspace/periods/{periodId}", "")
	register("UpdatePeriod", "periods.update", "PUT", "nixgonspace/periods/{periodId}", "")
	register("DeletePeriod", "periods.delete", "DELETE", "nixgonspace/periods/{periodId}", "")
	register("ListPeriods", "periods.list", "GET", "nixgonspace/periods", "")

	register("InsertTravel", "travels.insert", "POST", "nixgonspace/travels", "")
	register("GetTravel", "travels.get", "GET", "nixgonspace/travels/{travelId}", "")
	register("UpdateTravel", "travels.update", "PUT", "nixgonspace/travels/{travelId}", "")
	register("DeleteTravel", "travels.delete", "DELETE", "nixgonspace/travels/{travelId}", "")
	register("ListTravels", "travels.list", "GET", "nixgonspace/travels", "")

	register("InsertOrder", "orders.insert", "POST", "nixgonspace/orders", "")

	register("InsertBang", "bang.insert", "POST", "nixgonspace/bang", "")
	register("UpdateBang", "bang.update", "PUT", "nixgonspace/bang", "")

	register("InsertSMSMsg", "sms.insert", "POST", "nixgonspace/sms", "")
	register("InsertMail", "mail.insert", "POST", "nixgonspace/mail", "")
	endpoints.HandleHTTP()
}
